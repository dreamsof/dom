package com.th.dom.service.impl;

import com.th.dom.dao.UserMapper;
import com.th.dom.domain.User;
import com.th.dom.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;
    public User selectUserByName(String name) {
        return userMapper.selectUserByName(name);
    }
}
