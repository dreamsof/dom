package com.th.dom.service;


import com.th.dom.domain.User;

public interface UserService {
    /**
     *  根据用户名称查询用户信息
     * @param name
     * @return
     */
    User selectUserByName(String name);
}

