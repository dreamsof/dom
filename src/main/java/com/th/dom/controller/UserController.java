package com.th.dom.controller;

import com.th.dom.domain.User;
import com.th.dom.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @Package:        com.th.dom.controller
 * @ClassName:      UserController
 * @Description:    UserController
 * @Author:         dove
 * @CreateDate:     2018/7/17 15:38
 * @Version:        1.0
 * <p>Copyright: Copyright (c) 2018/7/17</p>
 *
 */
@Controller
@RequestMapping("/user")
public class UserController {
    private final Logger log = LoggerFactory.getLogger(UserController.class);
    //依赖注入
    @Autowired
    private UserService userService;
        /**
             * @Description:    通过用户名查询用户信息
             * @author      dove
             * @return
             * @exception
             * @date        2018/7/17 15:43
             */
    @RequestMapping("/selectUserByName")
    @ResponseBody
    public User selectUserByName(){
        User user = userService.selectUserByName("张三");
        log.info("UserController selectUserByName success----info!");
        log.error("UserController selectUserByName success----error!");
        return user;
    }

    @RequestMapping("/hello")
    @ResponseBody
    String home() {
        return "hello world";
    }
}
